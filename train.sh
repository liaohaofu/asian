LAYERS=${1:-50}
CAFFE=/u/hliao6/software/caffe/build/tools/caffe
SOLVER=models/ResNet-$LAYERS-solver.prototxt
WEIGHTS=models/ResNet-$LAYERS-model.caffemodel
LOG=log/resnet.log
$CAFFE train -solver $SOLVER -weights $WEIGHTS -gpu 0,1 2>&1 | tee $LOG 
