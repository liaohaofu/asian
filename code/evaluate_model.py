import sys
import caffe
import argparse
import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix

def print_progress(current, total):
    sys.stdout.write("\rTesting...{:.2f}%".format(
        current * 100.0 / total
    ))
    sys.stdout.flush()


def parse_arguments():
    """Parse arguments from command line
    """

    description = "Evaluate the performance of trained model"
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument(
        "--model", "-m",
        default="models/ResNet-50-deploy.prototxt",
        help="The model definition of the evaluation net"
    )

    parser.add_argument(
        "--mean_file", "-f",
        default="data/imagenet_mean.npy",
        help="The image mean of the training set or pretrained set"
    )

    parser.add_argument(
        "--source", "-s",
        default="data/test.txt",
        help="The list of test images that will be used for evaluation"
    )

    parser.add_argument(
        "weights_file",
        help="The trained model that will be evaluated"
    )

    parser.add_argument(
        "--gpu", "-g",
        type=int, default=0,
        help="The index of gpu that will be used"
    )

    parser.add_argument(
        "--iterations", "-i",
        type=int, default=-1,
        help="Number of test interations"
    )

    return parser.parse_args()

args = parse_arguments()

# Load test images from source
with open(args.source) as f:
    raw_list = [l.strip().split(' ') for l in f]
    image_list = [(l[0], int(l[1])) for l in raw_list]
    if args.iterations != -1:
        image_list = image_list[:args.iterations]
image_files, labels = zip(*image_list)

# Create a net object and its related transfomer
caffe.set_mode_gpu()
caffe.set_device(args.gpu)

net = caffe.Net(args.model, args.weights_file, caffe.TEST)

transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
image_mean = np.load(args.mean_file).mean(1).mean(1)
transformer.set_transpose('data', (2,0,1))
transformer.set_mean('data', image_mean)
transformer.set_raw_scale('data', 255)
transformer.set_channel_swap('data', (2,1,0))

# Forward the net and obtain the probs
probs = []
for image_file, label in image_list:
    origin_image = caffe.io.load_image(image_file)
    trans_image = transformer.preprocess('data', origin_image)
    net.blobs['data'].data[...] = trans_image
    output = net.forward()
    prob = np.copy(output['prob'][0])
    probs.append(prob)
    print_progress(len(probs), len(image_list))

# Analysis the probs and display results
label_set = sorted(list(set(labels)))
ranks = np.array([p.argsort()[::-1] for p in probs])
labels = np.array(labels)
predicts = ranks[:, 0]
hits = [1 if l == p else 0 for l, p in zip(labels, predicts)]
print('\nAccuracy: {:.2f}%'.format(sum(hits) * 100.0 / len(hits)))

cm = confusion_matrix(labels, predicts)
cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

pd.options.display.float_format = '{:.2f}%'.format
df = pd.DataFrame(cm_normalized)
df.columns = label_set
df.index = label_set
print('\nConfusion Matrix:\n' + df.multiply(100).to_string())

