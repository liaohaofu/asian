import os
import argparse
from os.path import join
from random import shuffle

def parse_arguments():
    """Parse arguments from command line
    """

    description = "Generate image and label lists for the Asian face dataset"
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument(
        "--image_folder", "-d",
        default="data/images",
        help="the folder that contains the Asian face dataset"
    )

    parser.add_argument(
        "--split", "-s",
        type=float, nargs=3, default=[0.8, 0.1, 0.1],
        help="the split of train, test and val set"
    )
    parser.add_argument(
        "--output_folder", "-o",
        default="data/",
        help="the output folder that will store generated sources"
    )

    return parser.parse_args()

args = parse_arguments()

tags = ["China", "Japan", "Korea"]

image_list = []
for label, tag in enumerate(tags):
    tag_folder = join(args.image_folder, tag)
    tag_list = [
        (join(tag_folder, f), label) for f in os.listdir(tag_folder)
        if join(tag_folder, f)
    ]
    image_list += tag_list

shuffle(image_list)
train_end = int(len(image_list) * args.split[0])
test_end = int(len(image_list) * (args.split[0] + args.split[1]))

train = image_list[:train_end]
test = image_list[train_end:test_end]
val = image_list[test_end:]

raw_image_list = [i + ' ' + str(l) for i, l in image_list]
with open(join(args.output_folder, 'images.txt'), 'w') as f:
    f.write('\n'.join(raw_image_list))

datasets = [train, test, val]
dataset_names = ['train', 'test', 'val']
for dataset, dataset_name in zip(datasets, dataset_names):
    raw_list = [i + ' ' + str(l) for i, l in dataset]
    output_file = join(args.output_folder, dataset_name + '.txt')
    with open(output_file, 'w') as f:
        f.write("\n".join(raw_list))

raw_label_list = [tag + ' ' + str(i) for i, tag in enumerate(tags)]
with open(join(args.output_folder, 'labels.txt'), 'w') as f:
    f.write("\n".join(raw_label_list))
