import os
import errno
import argparse
from os.path import join


def symlink_force(target, link_name):
    try:
        os.symlink(target, link_name)
    except OSError, e:
        if e.errno == errno.EEXIST:
            os.remove(link_name)
            os.symlink(target, link_name)
        else:
            raise e


def parse_argument():
    """Parse argument from command line.
    """

    description = "Relink dataset so that it matches the dataset locally"
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument(
        "--linked_dataset", "-k",
        default="data/images/",
        help="the dataset that we want to be relinked"
    )

    parser.add_argument(
        "--local_dataset", "-c",
        default="/home/haofu/Research/datasets/asian_faces/",
        help="the dataset that we want to relink with"
    )

    return parser.parse_args()

args = parse_argument()

countries = ['China', 'Japan', "Korea"]
for c in countries:
    country_folder = join(args.linked_dataset, c)
    for f in os.listdir(country_folder):
        link = join(country_folder, f)
        if os.path.islink(link):
            image_file = os.readlink(link)
            items = image_file.split('/' + c + '/')
            items[0] = join(args.local_dataset, c)
            image_file = join(*items)
            symlink_force(image_file, link)
