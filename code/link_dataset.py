import os
import shutil
import argparse
from os.path import join


def parse_arguments():
    """Parse arguments from command line
    """

    description = "Rename and create image links for the original asian "
    "dataset."
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument(
        "--input_folder", "-i",
        default="/localdisk/scratch/Project-Asia",
        help="The original asian face folder"
    )

    parser.add_argument(
        "--output_folder", "-o",
        default="data/images",
        help="The output folder for the generated image links"
    )

    return parser.parse_args()

args = parse_arguments()

countries = ["China", "Japan", "Korea"]
if os.path.isdir(args.output_folder):
    shutil.rmtree(args.output_folder)
os.mkdir(args.output_folder)

for c in countries:
    input_country_folder = join(args.input_folder, c)
    output_country_folder = join(args.output_folder, c)
    os.mkdir(output_country_folder)

    cnt = 0
    for f in os.listdir(input_country_folder):
        if os.path.isfile(join(input_country_folder, f)):
            image_extension = os.path.splitext(f)[-1]
            origin_image_file = join(input_country_folder, f)
            generated_image_link = join(
                output_country_folder, str(cnt) + image_extension
            )
            os.symlink(origin_image_file, generated_image_link)
            cnt += 1

