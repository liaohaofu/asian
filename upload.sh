#!/usr/bin/bash

# Upload binaries to google drive (for local use only)
GDRIVE=/u/hliao6/gdrive
PROJECT=$GDRIVE/Public/asian
if [ -d $PROJECT ]; then
    rm -rf $PROJECT 
fi

mkdir -p $PROJECT/log
cp log/*.log $PROJECT/log/ 
cp -r log/weights/ $PROJECT/log

mkdir -p $PROJECT/models
cp models/*.caffemodel $PROJECT/models

tar -czvf $GDRVE/Public/asian.tar.gz $GDRIVE/Public/asian/
rm -r $PROJECT

drive push $GDRIVE/Public 
