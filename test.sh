WEIGHTS_FOLDER=${1:-"log/weights/"}
ITERS=${2:-"-1"}
WEIGHTS_FILE=$(python -c "import os; import glob; print sorted(glob.glob('$WEIGHTS_FOLDER*.caffemodel'), key=os.path.getmtime)[-1]")
python code/evaluate_model.py $WEIGHTS_FILE -i $ITERS

